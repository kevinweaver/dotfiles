dotfiles
========

My very own personal dotfiles

/colorschemes  - Gnome Terminal Colorscheme scripts.  Should be moved to scripts repo. <br/>
/config_files - Powerline Config Files, made some customizations here <br/>
/.fontconfig   - Font config files <br/>
/.fonts        - A bunch of sick fonts! <br/>
/.git          - so meta dude <br/>
/install.sh    - THE INSTALL SCRIPT FOR THESE BAD BOYS <br/>
/.irssi        - IRSSI IRC config file <br/>
/.tmux         - tmux files <br/>
/.tmux.conf    - tmux config <br/>
/.vimrc        - vim config <br/>
/.zsh_aliases - zsh aliases.  Need to pull some from .zshrc <br/>
/.zshrc        - Zshell config. <br/>

